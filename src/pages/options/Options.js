import React from 'react';
import {
  Link
} from 'react-router-dom';

export default function Options() {
  return (
    <div>
      <h1>Options</h1>
      <ul>
        <li><Link to="/">Back</Link></li>
      </ul>
    </div>
  );
}
