import React, {Component} from 'react';
import {
  Link
} from 'react-router-dom';
import ListMenu from '../components/menu/ListMenu';
import './Main.css';

export default class Main extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <h1>Main Menu</h1>
        <ListMenu
          items= {[
            {
              key: 'item1',
              linkKey: '',
              name: 'Item 1',
              focused: true,
            },
            {
              key: 'item2',
              linkKey: '',
              name: 'Item 2',
            },
            {
              key: 'options',
              linkKey: '/options',
              name: 'Options',
            },
          ]}
        />
      </div>
    );
  }
}