import React from 'react';
import {
  Route,
  HashRouter,
  Switch
} from 'react-router-dom';
import './App.css';
import InputManager from './components/core/InputManager';
import ActionManager from './components/core/ActionManager';
import Main from './pages/Main';
import Options from './pages/options/Options';

const App = () => {
  return (
    <div className="App">
      <HashRouter>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/options" component={Options} />
        </Switch>
      </HashRouter>
    </div>
  );
}

export default App;
