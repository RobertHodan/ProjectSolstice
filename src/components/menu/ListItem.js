import React, {Component} from 'react';
import {
  Link
} from 'react-router-dom';
import './ListItem.css';
import PropTypes from 'prop-types';

export default class ListItem extends Component {
  constructor(props) {
    super(props);

    this.baseClassName = 'ListItem';
    this.itemFocusedCb = props.itemFocusedCb;
    this.onMouseEnter = this._handleMouseEnter.bind(this);
    this.onMouseLeave = this._handleMouseLeave.bind(this);
  }

  render() {
    return (
      <li
        className={this.props.focused ? this.baseClassName + ' highlight' : this.baseClassName}
      >
        <Link
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}
          to={this.props.linkKey}
        > {this.props.name}
        </Link>
      </li>
    );
  }

  // Public
  toggleFocus(bool = !this.state.focused) {
    this.setState({focused: bool});

    // If true, callback with the id of the item that is focused
    if (bool) this.itemFocusedCb(this.props.id);
    else this.itemFocusedCb();
  }

  // Private
  _handleMouseEnter(e) {
    this.toggleFocus(true);
  }

  _handleMouseLeave(e) {
    this.toggleFocus(false);
  }
}

ListItem.propTypes = {
  focused: PropTypes.bool,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  linkKey: PropTypes.string.isRequired,
}