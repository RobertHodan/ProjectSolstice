import React, {Component} from 'react';
import ListItem from './ListItem';
import './ListMenu.css';
import PropTypes from 'prop-types';

export default class ListMenu extends Component {
  constructor(props) {
    super();

    this.state = {
      items: props.items
    }
    // debugging
    window.listMenu = this;
  }

  render() {
    let listItems = this.state.items.map((itemProps, i) => {
      let listItem = <ListItem
        id= {i}
        key= {itemProps.key}
        focused= {itemProps.focused}
        name= {itemProps.name}
        linkKey= {itemProps.linkKey}
        itemFocusedCb = {this._itemFocused.bind(this)}
      />
      return listItem;
    });

    return (
      <ul className="ListMenu">
        {listItems}
      </ul>
    );
  }

  // Private
  _itemFocused(id) {
    let items = this.state.items.slice(0);

    for(let i=0; i<items.length; i++) {
      let item = items[i];
      item.focused = false;
      if (id === i) item.focused = true;
    }

    this.setState({items: items});
  }
}

ListMenu.propTypes = {
  items: PropTypes.array.isRequired,
}